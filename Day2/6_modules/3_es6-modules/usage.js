// import helloF1 from './file1.js';
// import helloF2 from './file2.js';
// import { check } from './file3.js';

// helloF1();
// helloF2();
// check()

import { app } from './app-modules/index.js';

console.log("Square: ", app.square(5));
console.log("Check: ", app.check(5));
console.log("Test: ", app.test(5));   