var file1 = file1 || {};

((f1) => {
    function hello() {
        console.log("Hello from file1.js");
    }

    f1.hello = hello;
})(file1);