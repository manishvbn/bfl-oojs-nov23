var file2 = file2 || {};

((f2) => {
    function hello() {
        console.log("Hello from file2.js");
    }

    f2.hello = hello;
})(file2);