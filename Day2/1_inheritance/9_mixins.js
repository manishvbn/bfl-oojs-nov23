// function Person(name, surname) {
//     this.name = name;
//     this.surname = surname;
// }

// function Developer(name, surname, knownLanguage) {
//     Person.apply(this, arguments);
//     this.knownLanguage = knownLanguage;
// }

// function Student(name, surname, subjectOfStudy) {
//     Person.apply(this, arguments);
//     this.subjectOfStudy = subjectOfStudy;
// }

// function DevStudent(name, surname, knownLanguage, subjectOfStudy) {
//     Developer.call(this, name, surname, knownLanguage);
//     Student.call(this, name, surname, subjectOfStudy);
// }

// DevStudent.prototype = Object.create(Developer.prototype);
// DevStudent.prototype.constructor = DevStudent;

// var johnSmith = new DevStudent("John", "Smith", "C#", "JavaScript");

// console.log(johnSmith.knownLanguage);		//result: C#
// console.log(johnSmith.subjectOfStudy);		//result: JavaScript

// console.log(johnSmith instanceof DevStudent);
// console.log(johnSmith instanceof Developer);
// console.log(johnSmith instanceof Student);

// -----------------------------------

function Person(name, surname) {
    this.name = name;
    this.surname = surname;
}

// var myMixin = {
//     getFullName: function () {
//         return this.name + " " + this.surname;
//     }
// };

// Object.assign(Person.prototype, myMixin);

// function augment(destination, source) {
//     for (var methodName in source) {
//         if (source.hasOwnProperty(methodName)) {
//             destination[methodName] = source[methodName];
//         }
//     }
//     return destination;
// }

// select members to add from a mixin
function augment(destination, source, ...methodNames) {
    if (methodNames) {
        for (var methodName of methodNames) {
            if (source.hasOwnProperty(methodName)) {
                destination[methodName] = source[methodName];
            }
        }
    } else {
        for (var methodName in source) {
            if (source.hasOwnProperty(methodName)) {
                destination[methodName] = source[methodName];
            }
        }
    }
    return destination;
}

var nameMixin = {
    getFullName: function () {
        return this.name + " " + this.surname;
    },
    capitalizeName: function () {
        return this.name.toUpperCase();
    }
};

// Object.assign(Person.prototype, nameMixin);
augment(Person.prototype, nameMixin, "getFullName");
augment(Person.prototype, movingMixin, "goLeft", "goRight");
augment(Person.prototype, studyingMixin, "readTopic", "writeTopic", "repeatTopic");

var johnSmith = new Person("John", "Smith");
console.log(johnSmith.getFullName());
console.log(johnSmith.capitalizeName());

class Employee extends mixNameMixin(mixMovingMixin(Person)) {

}