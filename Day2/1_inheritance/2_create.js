// Can we create objects without a prototype?
// Can we create object with a specific prototype?

// var myObject = Object.create(null);
// console.log(Object.getPrototypeOf(myObject));

// var person = { name: "John", surname: "Smith"};
// var developer = Object.create(person, {
//     knownLanguage: { value: "JavaScript" }
// });

// ECMAScript 2015 Method (Object.setPrototypeOf)
var person = { name: "John", surname: "Smith" };
var developer = { knownLanguage: "JavaScript" };
Object.setPrototypeOf(developer, person);

console.log(developer.name);
console.log(developer.surname);
console.log(developer.knownLanguage);
