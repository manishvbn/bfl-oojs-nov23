var Person = (function () {
    function capitalize(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function PersonConstructor(name, surname, protected) {
        protectedMembers = protected || {};
        protectedMembers.capitalize = capitalize;

        this.name = capitalize(name);
        this.surname = capitalize(surname);
    }

    return PersonConstructor;
})();

// var p = new Person("john", "smith");
// console.log(p.name);

function Developer(name, surname, knownLanguage) {
    var parentProtected = {};
    Person.call(this, name, surname, parentProtected);
    this.knownLanguage = parentProtected.capitalize(knownLanguage);
}

Developer.prototype = Object.create(Person.prototype);
Developer.prototype.constructor = Developer;

var p = new Developer("john", "smith", "javascript");
console.log(p.name);
console.log(p.surname);
console.log(p.knownLanguage);