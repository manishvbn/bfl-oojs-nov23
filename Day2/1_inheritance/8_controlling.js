// var person = { name: "John", surname: "Smith"};

// // Object.preventExtensions(person);
// // Object.seal(person);
// Object.freeze(person);

// person.age = 32;
// delete person.surname;
// person.name = "Manish";

// console.log(person);

// -----------------------------

function Person(name, surname) {
    this.name = name;
    this.surname = surname;

    // Object.preventExtensions(this);
    // Object.seal(this);
    Object.freeze(this);
}

function Developer(name, surname, knownLanguage) {
    Person.apply(this, arguments);
    this.knownLanguage = knownLanguage;
}

Developer.prototype = Object.create(Person.prototype);
Developer.prototype.constructor = Developer;

var dev = new Developer("Mario", "Rossi", "JavaScript");

dev.name = "Manish";
delete dev.surname;
console.log(dev);