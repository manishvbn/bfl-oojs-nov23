function Person(name, surname) {
    this.name = name;
    this.surname = surname;
}

class Developer extends Person {
    constructor(name, surname, knownLanguage) {
        super(name, surname);
        this.knownLanguage = knownLanguage;
    }
}

var johnSmith = new Developer("John", "Smith", "JavaScript");

console.log(johnSmith.name);			//result:	"John"
console.log(johnSmith.surname);			//result:	"Smith"
console.log(johnSmith.knownLanguage);		//result:	"JavaScript"

console.log(johnSmith instanceof Developer);
console.log(johnSmith instanceof Person);
console.log(johnSmith instanceof Object);