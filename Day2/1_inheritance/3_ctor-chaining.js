function Person(name, surname) {
    this.name = name;
    this.surname = surname;
}

function Developer(name, surname, knownLanguage) {
    Person.apply(this, arguments);
    this.knownLanguage = knownLanguage;
}

Developer.prototype = Object.create(Person.prototype);
Developer.prototype.constructor = Developer;

console.log(Developer.prototype.constructor);

var johnSmith = new Developer("John", "Smith", "JavaScript");

console.log(johnSmith.name);			//result:	"John"
console.log(johnSmith.surname);			//result:	"Smith"
console.log(johnSmith.knownLanguage);		//result:	"JavaScript"

console.log(johnSmith instanceof Developer);
console.log(johnSmith instanceof Person);
console.log(johnSmith instanceof Object);
