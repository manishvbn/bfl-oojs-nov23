Object.prototype.implementsMethod = function (method) {
    return !!(this[method] && this[method] instanceof Function)
};

Object.prototype.implementsProperty = function (property) {
    return !!(this[property] && !(this[property] instanceof Function))
};

class SoftwareHouse {
    constructor() {
        this.employees = [];
    }

    hire(dev) {
        if (dev.implementsMethod("writeCode") && dev.implementsProperty("name")) {
            this.employees.push(dev);
        } else {
            throw new Error("The argument is not compatible with the required interface");
        }
    }

    createSoftware() {
        var newSoftware = [];
        var employee;
        var module;

        for (var i = 0; i < this.employees.length; i++) {
            employee = this.employees[i];
            module = employee.writeCode();
            newSoftware.push(module);
        }

        return newSoftware;
    }
}

var johnSmith = { name1: "John", surname: "Smith", writeCode: function () { } };

// console.log(johnSmith.implementsProperty("name"));		//result: true
// console.log(johnSmith.implementsMethod("writeCode"));	//result: true
// console.log(johnSmith.implementsMethod("writePoems"));	//result: false

var swHouse = new SoftwareHouse();
swHouse.hire(johnSmith);
console.log(swHouse.employees.length);