// class Developer {
//     constructor(name, surname) {
//         this.name = name;
//         this.surname = surname;
//     }
// }

// class Salesman {
//     constructor(name, surname) {
//         this.firstName = name;
//         this.secondName = surname;
//     }
// }

// class BusinessAnalyst {
//     constructor(fullName) {
//         this.fullName = fullName;
//     }
// }

// class SoftwareHouse {
//     constructor() {
//         this.employees = [];
//     }

//     listEmployees() {
//         var employeesLen = this.employees.length;
//         var currentEmployee;

//         for (var i = 0; i < employeesLen; i++) {
//             currentEmployee = this.employees[i];

//             if (currentEmployee instanceof Developer) {
//                 console.log(currentEmployee.name + " " + currentEmployee.surname);
//             } else if (currentEmployee instanceof Salesman) {
//                 console.log(currentEmployee.firstName + " " + currentEmployee.secondName);
//             } else if (currentEmployee instanceof BusinessAnalyst) {
//                 console.log(currentEmployee.fullName);
//             }
//         }
//     }
// }

// var swHouse = new SoftwareHouse();
// swHouse.employees.push(new Developer("John", "Smith"));
// swHouse.employees.push(new Salesman("Mario", "Rossi"));
// swHouse.employees.push(new BusinessAnalyst("Susane Smith"));
// swHouse.listEmployees();

// -------------------------------------------

class Interface {
    constructor(name, methods = [], properties = []) {
        this.name = name;
        this.methods = [];
        this.properties = [];

        for (let i = 0, len = methods.length; i < len; i++) {
            if (typeof methods[i] !== 'string') {
                throw new Error("Interface constructor expects method names to be passed in as a string.");
            }
            this.methods.push(methods[i]);
        }

        for (let i = 0, len = properties.length; i < len; i++) {
            if (typeof properties[i] !== 'string') {
                throw new Error("Interface constructor expects property names to be passed in as a string.");
            }
            this.properties.push(properties[i]);
        }
    }

    isImplementedBy(obj) {
        var methodsLen = this.methods.length;
        var propertiesLen = this.properties.length;
        var currentMember;

        if (obj) {
            //check methods
            for (let i = 0; i < methodsLen; i++) {
                currentMember = this.methods[i];
                if (!obj[currentMember] || typeof obj[currentMember] !== "function") {
                    throw new Error("The object does not implement the interface " + this.name + ". Method " + currentMember + " not found.");
                }
            }

            //check properties
            for (let i = 0; i < propertiesLen; i++) {
                currentMember = this.properties[i];
                if (!obj[currentMember] || typeof obj[currentMember] === "function") {
                    throw new Error("The object does not implement the interface " + this.name + ". Property " + currentMember + " not found.");
                }
            }
        } else {
            throw new Error("No object to check!");
        }
    }
}

class Developer {
    constructor(name, surname) {
        this.name = name;
        this.surname = surname;
    }
}

class Salesman {
    constructor(name, surname) {
        this.firstName = name;
        this.secondName = surname;
    }
}

class BusinessAnalyst {
    constructor(fullName) {
        this.fullName = fullName;
    }
}

var IFullName = new Interface("IFullName", ["getFullName"]);

class SoftwareHouse {
    constructor() {
        this.employees = [];
    }

    hire(dev) {
        IFullName.isImplementedBy(dev);
        this.employees.push(dev);
    }

    listEmployees() {
        var employeesLen = this.employees.length;
        var currentEmployee;
        for (var i = 0; i < employeesLen; i++) {
            currentEmployee = this.employees[i];
            console.log(currentEmployee.getFullName());
        }
    }
}

var swHouse = new SoftwareHouse();
swHouse.employees.push(new Developer("John", "Smith"));
swHouse.employees.push(new Salesman("Mario", "Rossi"));
swHouse.employees.push(new BusinessAnalyst("Susane Smith"));
swHouse.listEmployees();
