// class SoftwareHouse {
//     constructor() {
//         this.employees = [];
//     }

//     hire(dev) {
//         if (dev && dev["writeCode"] && dev["writeCode"] instanceof Function) {
//             this.employees.push(dev);
//         } else {
//             throw new Error("The argument do not implements writeCode method")
//         }
//     }

//     createSoftware() {
//         var newSoftware = [];
//         var employee;
//         var module;

//         for (var i = 0; i < this.employees.length; i++) {
//             employee = this.employees[i];
//             module = employee.writeCode();
//             newSoftware.push(module);
//         }

//         return newSoftware;
//     }
// }

// // ------------------------------------------------- Better Implementation

// var SoftwareHouse = (function () {
//     function implement(obj, method) {
//         return (obj && obj[method] && obj[method] instanceof Function);
//     }

//     return class {
//         constructor() {
//             this.employees = [];
//         }

//         hire(dev) {
//             if (implement(dev, "writeCode")) {
//                 this.employees.push(dev);
//             } else {
//                 throw new Error("The argument do not implements writeCode method")
//             }
//         }

//         createSoftware() {
//             var newSoftware = [];
//             var employee;
//             var module;

//             for (var i = 0; i < this.employees.length; i++) {
//                 employee = this.employees[i];
//                 module = employee.writeCode();
//                 newSoftware.push(module);
//             }

//             return newSoftware;
//         }
//     }
// })();

// ------------------------------------------------- Multi Check Implementation

var SoftwareHouse = (function () {
    function implementsMethod(obj, method) {
        return !!(obj && obj[method] && obj[method] instanceof Function);
    }

    function implementsProperty(obj, property) {
        return !!(obj && obj[property] && !(obj[property] instanceof Function))
    }

    return class {
        constructor() {
            this.employees = [];
        }

        hire(dev) {
            if (implementsMethod(dev, "writeCode") && implementsProperty(dev, "name")) {
                this.employees.push(dev);
            } else {
                throw new Error("The argument do not implements writeCode method")
            }
        }

        createSoftware() {
            var newSoftware = [];
            var employee;
            var module;

            for (var i = 0; i < this.employees.length; i++) {
                employee = this.employees[i];
                module = employee.writeCode();
                newSoftware.push(module);
            }

            return newSoftware;
        }
    }
})();