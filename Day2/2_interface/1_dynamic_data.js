// function square(n) {
//     return n * n;
// }

// console.log(square(3));
// console.log(square("3"));
// console.log(square("three"));
// console.log(square(true));
// console.log(square({ a: 2 }));

// -------------------------------

function square(n) {
    var result;

    if (typeof n === "number") {
        result = n * n;
    } else {
        throw new Error("Argument must be a number");
    }

    return result;
}

console.log(square(3));
console.log(square("3"));
console.log(square("three"));
console.log(square(true));
console.log(square({ a: 2 }));
