// software house is interested in hiring people who are able to
// write code, not just any person.

// function Person(name, surname) {
//     this.name = name;
//     this.surname;
// }

// class SoftwareHouse {
//     constructor() {
//         this.employees = [];
//     }

//     hire(dev) {
//         if (dev instanceof Person)
//             this.employees.push(dev);
//         else
//             throw new Error("This software house hires only persons!");
//     }

//     createSoftware() {
//         var newSoftware = [];
//         var employee;
//         var module;

//         for (var i = 0; i < this.employees.length; i++) {
//             employee = this.employees[i];
//             module = employee.writeCode();
//             newSoftware.push(module);
//         }

//         return newSoftware;
//     }
// }

// var johnSmith = new Person("John", "Smith");

// var swHouse = new SoftwareHouse();
// swHouse.hire(johnSmith);
// swHouse.createSoftware();


// -----------------------------------------------------

// -------------------------
// // How can we impose this new constraint on the hire() method?

// Create Developer and Accept it in hire()
// we might fail to check whether people can be hired because of multiple inheritance.


function Person(name, surname) {
    this.name = name;
    this.surname;
}

function Developer(name, surname, knownLanguage) {
	Person.apply(this, arguments);
	this.knownLanguage =  knownLanguage;
}

Developer.prototype = Object.create(Person.prototype);
Developer.prototype.constructor = Developer;

function Student(name, surname, subjectOfStudy) {
	Person.apply(this, arguments);
	this.subjectOfStudy = subjectOfStudy;
}

Student.prototype = Object.create(Person.prototype);
Student.prototype.constructor = Student;

function DevStudent(name, surname, knownLanguage, subjectOfStudy) {
	Developer.call(this, name, surname, knownLanguage);
	Student.call(this, name, surname, subjectOfStudy);
}

DevStudent.prototype.writeCode = function() {
	console.log("writing code...");
	return {module: "..."};
};

var johnSmith = new DevStudent("John", "Smith", "C#", "JavaScript");

console.log(johnSmith instanceof Person);
console.log(johnSmith instanceof Developer);
console.log(johnSmith instanceof Student);