// const IdGenerator = (function () {
//     var counter = 0;
//     var instance;

//     function IdGeneratorConstructor() {
//         if(!instance){
//             instance = this;
//         }

//         return instance;
//     }

//     IdGeneratorConstructor.prototype.newId = function () {
//         return ++counter;
//     }

//     return IdGeneratorConstructor;
// })();

const IdGenerator = (function () {
    var counter = 0;
    var instance;

    return class {
        constructor() {
            if (!instance) {
                instance = this;
            }

            return instance;
        }

        newId() {
            return ++counter;
        }
    }
})();

var g = new IdGenerator();
console.log(g.newId());
console.log(g.newId());
console.log(g.newId());

var g1 = new IdGenerator();
console.log(g1.newId());
console.log(g1.newId());
console.log(g1.newId());

console.log(g === g1);