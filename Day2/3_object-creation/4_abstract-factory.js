class Developer {
    constructor(skills, benefits) {
        this.skills = ["programming"].concat(skills);
        this.salary = 40000;
        this.benefits = ["computer"].concat(benefits);
    }
}

class Salesman {
    constructor(skills, benefits) {
        this.skills = ["selling"].concat(skills);
        this.salary = 50000;
        this.benefits = ["computer"].concat(benefits);
    }
}

class BusinessAnalyst {
    constructor(skills, benefits) {
        this.skills = ["analyzing"].concat(skills);
        this.salary = 60000;
        this.benefits = ["computer"].concat(benefits);
    }
}

class DevAgency {
    getStaffMember(skills, benefits) {
        return new Developer(skills, benefits);
    }
}

class SalesAgency {
    getStaffMember(skills, benefits) {
        return new Salesman(skills, benefits);
    }
}

class BusinessAnalystAgency {
    getStaffMember(skills, benefits) {
        return new BusinessAnalyst(skills, benefits);
    }
}

class RecruitmentAgencyAbstractFactory {
    constructor() {
        this.agencyFactories = {};
    }

    register(area, agencyFactory) {
        this.agencyFactories[area] = agencyFactory;
    }

    getAgency(area) {
        return new this.agencyFactories[area]();
    }
}

class SoftwareHouse {
    constructor(recruitmentAgencyFactory) {
        this.employees = [];
        this.agencyFinder = recruitmentAgencyFactory;
        this.devAgency = this.agencyFinder.getAgency("dev");
        this.saleAgency = this.agencyFinder.getAgency("sale");
        this.baAgency = this.agencyFinder.getAgency("ba");
    }

    hireDeveloper() {
        var dev = this.devAgency.getStaffMember("dev", ["JavaScript"], ["smartphone"]);
        this.employees.push(dev);
    }

    hireSalesman() {
        var sm = this.saleAgency.getStaffMember("sale", ["communication"], ["smartphone", "car"]);
        this.employees.push(sm);
    }

    hireBusinessAnalyst() {
        var ba = this.baAgency.getStaffMember("ba", ["communication", "writing"], ["smartphone", "tablet"]);
        this.employees.push(ba);
    }
}

var agencyAbstractFactory = new RecruitmentAgencyAbstractFactory();
agencyAbstractFactory.register("dev", Developer);
agencyAbstractFactory.register("sale", Salesman);
agencyAbstractFactory.register("ba", BusinessAnalyst);

var swHouse = new SoftwareHouse(agencyAbstractFactory);