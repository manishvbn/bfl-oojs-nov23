// class Developer {
// 	constructor(skills, benefits) {
// 		this.skills = ["programming"].concat(skills);
// 		this.salary = 40000;
// 		this.benefits = ["computer"].concat(benefits);
// 	}
// }

// class SoftwareHouse {
// 	constructor() {
// 		this.employees = [];
// 	}

// 	hireDeveloper() {
// 		var dev = new Developer(["JavaScript"], ["smartphone"]);
// 		this.employees.push(dev);
// 	}
// }

// -------------------------------

// class Developer {
// 	constructor(skills, benefits) {
// 		this.skills = ["programming"].concat(skills);
// 		this.salary = 40000;
// 		this.benefits = ["computer"].concat(benefits);
// 	}
// }

// class Salesman {
//     constructor(skills, benefits) {
//         this.skills = ["selling"].concat(skills);
//         this.salary = 50000;
//         this.benefits = ["computer"].concat(benefits);
//     }
// }

// class BusinessAnalyst {
//     constructor(skills, benefits) {
//         this.skills = ["analyzing"].concat(skills);
//         this.salary = 60000;
//         this.benefits = ["computer"].concat(benefits);
//     }
// }

// class SoftwareHouse {
// 	constructor() {
// 		this.employees = [];
// 	}

// 	hireDeveloper() {
// 		var dev = new Developer(["JavaScript"], ["smartphone"]);
// 		this.employees.push(dev);
// 	}

//     hireSalesman() {
//         var sm = new Salesman(["communication"], ["smartphone", "car"]);
//         this.employees.push(sm);
//     }

//     hireBusinessAnalyst() {
//         var ba = new BusinessAnalyst(["communication", "writing"], ["smartphone", "tablet"]);
//         this.employees.push(ba);
//     }
// }

// --------------------------- Solution

class Developer {
    constructor(skills, benefits) {
        this.skills = ["programming"].concat(skills);
        this.salary = 40000;
        this.benefits = ["computer"].concat(benefits);
    }
}

class Salesman {
    constructor(skills, benefits) {
        this.skills = ["selling"].concat(skills);
        this.salary = 50000;
        this.benefits = ["computer"].concat(benefits);
    }
}

class BusinessAnalyst {
    constructor(skills, benefits) {
        this.skills = ["analyzing"].concat(skills);
        this.salary = 60000;
        this.benefits = ["computer"].concat(benefits);
    }
}

class Tester {
    constructor(skills, benefits) {
        this.skills = ["testing"].concat(skills);
        this.salary = 50000;
        this.benefits = ["computer"].concat(benefits);
    }
}

// class RecruitmentAgency {
//     getStaffMember(role, skills, benefits) {
//         var member;

//         switch (role.toLowerCase()) {
//             case "dev":
//                 member = new Developer(skills, benefits);
//                 break;
//             case "sale":
//                 member = new Salesman(skills, benefits);
//                 break;
//             case "ba":
//                 member = new BusinessAnalyst(skills, benefits);
//                 break;
//             default:
//                 throw new Error("Unable to hire people for the role " + role)
//         }

//         return member;
//     }
// }

// Using Ctor Registration

class RecruitmentAgency {
    constructor() {
        this.objConstructors = {};
    }

    register(role, constructor) { 
        this.objConstructors[role] = constructor;
    }

    getStaffMember(role, skills, benefits) {
        var objConstructor = this.objConstructors[role];
        var member;
        
        if(objConstructor) member = new objConstructor(skills, benefits);

        return member;
    }
}

class SoftwareHouse {
    constructor(recruitmentAgency) {
        this.employees = [];
        this.agency = recruitmentAgency;
    }

    hireDeveloper() {
        var dev = this.agency.getStaffMember("dev", ["JavaScript"], ["smartphone"]);
        this.employees.push(dev);
    }

    hireSalesman() {
        var sm = this.agency.getStaffMember("sale", ["communication"], ["smartphone", "car"]);
        this.employees.push(sm);
    }

    hireBusinessAnalyst() {
        var ba = this.agency.getStaffMember("ba", ["communication", "writing"], ["smartphone", "tablet"]);
        this.employees.push(ba);
    }

    hireTester() {
        var tester = this.agency.getStaffMember("test", ["JavaScript"], ["smartphone", "tablet"]);
        this.employees.push(tester);
    }
}

// var swHouse = new SoftwareHouse(new RecruitmentAgency());

var agency = new RecruitmentAgency();
agency.register("dev", Developer);
agency.register("sale", Salesman);
agency.register("ba", BusinessAnalyst);
agency.register("test", Tester);

var swHouse = new SoftwareHouse(agency);

// class Project{
//     constructor(employee) {
//         this.manager = employee;
//     }

//     // setManager(employee) {
//     //     this.manager = employee;
//     // }

//     set Manager(employee) {
//         this.manager = employee;
//     }
// }

// class Employee {

// }

// let e1 = new Employee();
// let project1 = new Project(e1);

// let e2 = new Employee();
// // project1.setManager(e2);
// project1.Manager = e2;