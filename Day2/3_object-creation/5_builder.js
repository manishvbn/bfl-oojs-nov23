class Developer {
    constructor(builder) {
        this.skills = ["programming"].concat(builder.skills);
        this.salary = builder.salary || 40000; // Default value if not provided
        this.benefits = ["computer"].concat(builder.benefits);
        this.experience = builder.experience;
        this.certifications = builder.certifications;
        // ... and so on for other attributes
    }
}

class DeveloperBuilder {
    constructor() {
        this.skills = [];
        this.benefits = [];
    }

    setSkills(skills) {
        this.skills = skills;
        return this;  // return the builder for method chaining
    }

    setBenefits(benefits) {
        this.benefits = benefits;
        return this;
    }

    setExperience(years) {
        this.experience = years;
        return this;
    }

    setCertifications(certifications) {
        this.certifications = certifications;
        return this;
    }

    build() {
        return new Developer(this);
    }
}

class DevAgency {
    getStaffMember({ skills, benefits, experience, certifications }) {
        return new DeveloperBuilder()
            .setSkills(skills)
            .setBenefits(benefits)
            .setExperience(experience)
            .setCertifications(certifications)
            .build();
    }
}

class RecruitmentAgencyAbstractFactory {
    constructor() {
        this.agencyFactories = {};
    }

    register(area, agencyFactory) {
        this.agencyFactories[area] = agencyFactory;
    }

    getAgency(area) {
        return new this.agencyFactories[area];
    }
}

class SoftwareHouse {
    constructor(recruitmentAgencyFactory) {
        this.employees = [];
        this.agencyFinder = recruitmentAgencyFactory;
        this.devAgency = this.agencyFinder.getAgency("dev");
        this.saleAgency = this.agencyFinder.getAgency("sale");
        this.baAgency = this.agencyFinder.getAgency("ba");
    }

    hireDeveloper() {
        var dev = this.devAgency.getStaffMember("dev", ["JavaScript"], ["smartphone"]);
        this.employees.push(dev);
    }
}

var agencyAbstractFactory = new RecruitmentAgencyAbstractFactory();
agencyAbstractFactory.register("dev", Developer);

var swHouse = new SoftwareHouse(agencyAbstractFactory);