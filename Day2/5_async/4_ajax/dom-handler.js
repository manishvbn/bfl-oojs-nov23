import postApiClient from "./post-api-client.js";
import getData from "./static-data.js";

var ajaxDiv = document.querySelector('#ajaxDiv');
var messageDiv = document.querySelector('#messageDiv');

if (ajaxDiv.style.display === "none") {
    ajaxDiv.style.display = "block";
    messageDiv.style.display = "none";
}

var button = document.createElement('button');
button.className = "btn btn-primary";
button.innerHTML = "Get Data";

var btnArea = document.querySelector('#btnArea');
btnArea.appendChild(button);

// // 1. Static Data Call
// button.addEventListener('click', function () {
//     var data = getData();
//     generateTable(data);
// });

// // 2. Using Callbacks
// button.addEventListener('click', function () {
//     postApiClient.getAllPostsUsingCallbacks(function (data) {
//         generateTable(data);
//     }, function (error) {
//         console.error(error);
//     });
// });

// // 3. Using Promise
// button.addEventListener('click', function () {
//     postApiClient.getAllPostsUsingPromise().then(function (data) {
//         generateTable(data);
//     }).catch(function (error) {
//         console.error(error);
//     });
// });

// // 4. Using Async Await
// button.addEventListener('click', async function () {
//     try {
//         var data = await postApiClient.getAllPostsUsingPromise();
//         generateTable(data);
//     } catch (error) {
//         console.error(error);
//     }
// });

// // 5. Using Async Function
// button.addEventListener('click', async function () {
//     try {
//         var data = await postApiClient.getAllPostsAsync();
//         generateTable(data);
//     } catch (error) {
//         console.error(error);
//     }
// });

// 6. Using Async Generator Function
button.addEventListener('click', async function () {
    const it = postApiClient.getAllPosts();

    try {
        var data = await it.next();
        generateTable(data.value);
    } catch (error) {
        console.error(error);
    }
});

function generateTable(data) {
    let table = document.getElementById("postTable");
    let row, cell;

    for (const item of data) {
        row = table.insertRow();
        cell = row.insertCell();
        cell.textContent = item.id;
        cell = row.insertCell();
        cell.textContent = item.title;
        cell = row.insertCell();
        cell.textContent = item.body;
    }
}