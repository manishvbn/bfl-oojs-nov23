// --------------------------------- Isolate Private Member using Weak Map
var TheatreSeats = (function () {
    var priv = new WeakMap();
    var _ = function (instance) { return priv.get(instance); };

    function TheatreSeatsConstructor() {
        var privateMembers = { seats: [] };

        priv.set(this, privateMembers);
        this.maxSize = 10;

    }

    TheatreSeatsConstructor.prototype.placePerson = function (person) {
        _(this).seats.push(person);
    };

    TheatreSeatsConstructor.prototype.countOccupiedSeats = function () {
        return _(this).seats.length;
    };

    TheatreSeatsConstructor.prototype.isSoldOut = function () {
        return _(this).seats.length >= this.maxSize;
    };

    TheatreSeatsConstructor.prototype.countFreeSeats = function () {
        return this.maxSize - _(this).seats.length;
    };

    return TheatreSeatsConstructor;
}());

var theatreSeats1 = new TheatreSeats();
var theatreSeats2 = new TheatreSeats();

theatreSeats1.placePerson("John Smith");

console.log(theatreSeats1.countOccupiedSeats());
console.log(theatreSeats2.countOccupiedSeats());

// var wmap = new WeakMap();

// wmap.set({ id: 1 }, "Value 1");
// wmap.set({ id: 2 }, "Value 1");

// var map = new WeakMap();

// function f1() {
//     var obj1 = { id: 1 };
//     var obj2 = { id: 2 };

//     map.set(obj1, "Value 1");
//     map.set(obj2, "Value 1");
// }

// f1();

// ------------------------------------------- ES6 Class

var TheatreSeats = (function () {
    var priv = new WeakMap();
    var _ = function (instance) { return priv.get(instance); };

    class TheatreSeatsClass {
        constructor() {
            var privateMembers = { seats: [] };
            priv.set(this, privateMembers);
            this.maxSize = 10;
        }

        placePerson(person) {
            _(this).seats.push(person);
        };

        countOccupiedSeats() {
            return _(this).seats.length;
        };

        isSoldOut() {
            return _(this).seats.length >= this.maxSize;
        };

        countFreeSeats() {
            return this.maxSize - _(this).seats.length;
        };
    }

    return TheatreSeatsClass;
}());