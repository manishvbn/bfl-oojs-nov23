// var person = {
//     name: "John",
//     surname: "Smith",
//     get fullName() { return this.name + " " + this.surname; },
// }

// console.log(person.fullName);

// person.fullName = "Mario Rossi";

// console.log(person.fullName);

// ------------------- with set

// var person = {
//     name: "John",
//     surname: "Smith",
//     get fullName() { return this.name + " " + this.surname; },
//     set fullName(value) {
//         var parts = value.split(" ");
//         this.name = parts[0];
//         this.surname = parts[1];
//     }
// }

// console.log(person.fullName);

// person.fullName = "Mario Rossi";

// console.log(person.fullName);


// ---------------------------------------

// var  Person = (function() {
// 	function PersonConstructor() {
// 		this.name = "";
// 		this.surname = "";
//     }

//     Object.defineProperty(
//         this,
//         "fullName",
//         {
//             get: function() { return this.name + " " + this.surname; },
//             set: function(value) {
//                 var parts = value.split(" ");
//                 this.name = parts[0];
//                 this.surname = parts[1];
//             }
//         }
//     )

//     return  PersonConstructor;
// }());

// var person = new Person();
// console.log(person.fullName);

// person.fullName = "Mario Rossi";

// console.log(person.fullName);
// // console.log("Name: ", person.name);

// --------------------------------------------

// var Person = (function () {
//     var priv = new WeakMap();
//     var _ = function (instance) { return priv.get(instance); };

//     function PersonConstructor() {
//         var privateMembers = { email: "" };

//         priv.set(this, privateMembers);

//         this.name = "";
//         this.surname = "";
//     }

//     Object.defineProperty(
//         PersonConstructor.prototype,
//         "fullName",
//         {
//             get: function () { return this.name + " " + this.surname; }
//         });

//     Object.defineProperty(
//         PersonConstructor.prototype,
//         "email",
//         {
//             get: function () { return _(this).email; },
//             set: function (value) {
//                 var emailRegExp = /\w+@\w+\.\w{2,4}/i;

//                 if (emailRegExp.test(value)) {
//                     _(this).email = value;
//                 } else {
//                     throw new Error("Invalid email address!");
//                 }
//             }
//         });

//     return PersonConstructor;

// }());
// ----------------------------------------

var Person = (function () {
    var priv = new WeakMap();
    var _ = function (instance) { return priv.get(instance); };

    class PersonClass {
        constructor() {
            var privateMembers = { email: "" };
            priv.set(this, privateMembers);
            this.name = "";
            this.surname = "";
        }

        get fullName() { return this.name + " " + this.surname; }

        get email() { return _(this).email; }

        set email(value) {
            var emailRegExp = /\w+@\w+\.\w{2,4}/i;

            if (emailRegExp.test(value)) {
                _(this).email = value;
            } else {
                throw new Error("Invalid email address!");
            }
        }
    }

    return PersonClass;
}());
