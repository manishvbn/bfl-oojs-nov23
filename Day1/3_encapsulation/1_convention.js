function TheatreSeats() {
    this._seats = [];
}

TheatreSeats.prototype.placePerson = function (person) {
    this._seats.push(person);
}

var theatreSeats = new TheatreSeats();
// theatreSeats.seats = "Hello";
theatreSeats.placePerson("John Smith");
console.log(theatreSeats._seats);
