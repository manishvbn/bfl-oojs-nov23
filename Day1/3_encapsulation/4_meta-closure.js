// const TheatreSeats = (function () {
//     var seats = [];

//     function TheatreSeatsConstructor() {
//         this.maxSize = 10;
//     }

//     TheatreSeatsConstructor.prototype.placePerson = function (person) {
//         seats.push(person);
//     }

//     TheatreSeatsConstructor.prototype.countOccupiedSeats = function () {
//         return seats.length;
//     };

//     TheatreSeatsConstructor.prototype.isSoldOut = function () {
//         return this.countOccupiedSeats() >= this.maxSize;
//     }

//     TheatreSeatsConstructor.prototype.countFreeSeats = function () {
//         return this.maxSize - this.countOccupiedSeats();
//     }

//     return TheatreSeatsConstructor;
// })();

// var theatreSeats = new TheatreSeats();
// theatreSeats.placePerson("John Smith");
// console.log(theatreSeats.isSoldOut());
// console.log(theatreSeats.countFreeSeats());

// var theatreSeats1 = new TheatreSeats();
// var theatreSeats2 = new TheatreSeats();
// console.log(theatreSeats1);
// console.log(theatreSeats2);

// var theatreSeats1 = new TheatreSeats();
// var theatreSeats2 = new TheatreSeats();

// theatreSeats1.placePerson("John Smith");

// console.log(theatreSeats1.countOccupiedSeats());
// console.log(theatreSeats2.countOccupiedSeats());


// --------------------------------- Isolate Private Member
const TheatreSeats = (function () {
    var priv = {};
    var id = 0;

    function TheatreSeatsConstructor() {
        this.id = id++;
        this.maxSize = 10;

        priv[this.id] = {};
        priv[this.id].seats = [];
    }

    TheatreSeatsConstructor.prototype.placePerson = function (person) {
        priv[this.id].seats.push(person);
    }

    TheatreSeatsConstructor.prototype.countOccupiedSeats = function () {
        return priv[this.id].seats.length;
    };

    TheatreSeatsConstructor.prototype.isSoldOut = function () {
        return this.countOccupiedSeats() >= this.maxSize;
    }

    TheatreSeatsConstructor.prototype.countFreeSeats = function () {
        return this.maxSize - this.countOccupiedSeats();
    }

    return TheatreSeatsConstructor;
})();

var theatreSeats1 = new TheatreSeats();
var theatreSeats2 = new TheatreSeats();

theatreSeats1.placePerson("John Smith");

console.log(theatreSeats1.countOccupiedSeats());
console.log(theatreSeats2.countOccupiedSeats());

// The first problem concerns the new id property. We need this property in order to identify
// each object instance and to access its private members. However, since it is a publicly
// accessible number, it represents a potential risk. If we change its value, even if accidentally,
// we incur unexpected behavior.

// The second problem regards again memory consumption. Since private members exist in
// the meta-closure, their life will coincide with the closure life. So, if we create an object using
// our constructor and then destroy the object, its private members will remain alive in the
// anonymous function's closure, wasting memory. No garbage collector will remove these
// members since it is not aware that they are not used.