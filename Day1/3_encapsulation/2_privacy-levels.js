// function TheatreSeats() {
//     var seats = [];

//     this.placePerson = function (person) {
//         seats.push(person);
//     }
// }

// var theatreSeats = new TheatreSeats();
// theatreSeats.placePerson("John Smith");

function TheatreSeats() {
    // Private
    var seats = [];

    // Privileged
    this.placePerson = function (person) {
        seats.push(person);
    }

    this.countOccupiedSeats = function () {
        return seats.length;
    };

    this.maxSize = 10;
}

// Public
TheatreSeats.prototype.isSoldOut = function () {
    return this.countOccupiedSeats() >= this.maxSize;
}

TheatreSeats.prototype.countFreeSeats = function () {
    return this.maxSize - this.countOccupiedSeats();
}

// var theatreSeats = new TheatreSeats();
// theatreSeats.placePerson("John Smith");
// console.log(theatreSeats.isSoldOut());
// console.log(theatreSeats.countFreeSeats());

// console.log(theatreSeats);

var theatreSeats1 = new TheatreSeats();
var theatreSeats2 = new TheatreSeats();

console.log(theatreSeats1);
console.log(theatreSeats2);