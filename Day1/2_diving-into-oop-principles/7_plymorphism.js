// // public int Sum(int x, int y){
// //     return x + y;
// // }

// // public int Sum(int x, int y, int z){
// //     return x + y + z;
// // }

// // function Sum(x, y, z) {
// //     x = x || 0;
// //     y = y || 0;
// //     z = z || 0;
// //     return x + y + z;
// // }

// function Sum(x = 0, y = 0, z = 0) {
//     return x + y + z;
// }

// console.log(Sum(1, 2, 3));
// console.log(Sum(1, 2));
// console.log(Sum(1));
// console.log(Sum());

// ---------------------------------------

// public class Stack<T> {
// 	private T[] items;
// 	private int count;
// 	public void Push(T item) { ... }
// 	public T Pop() { ... }
// }

// var stack = new Stack<string>();
// var stack = new Stack<int>();

// function Stack() {
//     this.items = [];
//     this.pop = function () {
//         return this.items.pop();
//     },
//     this.push = function (item) {
//         this.items.push(item);
//     }
// }

// ------------------------------------

//C#

// public class Person {
//     public string Name { get; set; }
//     public string SurName { get; set; }
//    }

// public class Programmer:Person {
//        public String KnownLanguage { get; set; }
// }
   
//    public void WriteFullName(Person p) {
//     Console.WriteLine(p.Name + " " + p.SurName);
// }

// var a = new Person();
// a.Name = "John";
// a.SurName = "Smith";

// var b = new Programmer();
// b.Name = "Mario";
// b.SurName = "Rossi";
// b.KnownLanguage = "C#";

// WriteFullName(a);	//result: John Smith
// WriteFullName(b);	//result: Mario Rossi

function Person(){
    this.name = "";
    this.surname = "";
}

function Programmer() {
    this.knownLanguage = "";
}
Programmer.prototype = new Person();

function writeFullName(p) {
    console.log(p.name + " " + p.surname);
}

var a = new Person();
a.name = "John";
a.surname = "Smith";

var b = new Programmer();
b.name = "Mario";
b.surname = "Rossi";
b.knownLanguage = "JavaScript";

writeFullName(a);	//result: John Smith
writeFullName(b);	//result: Mario Rossi