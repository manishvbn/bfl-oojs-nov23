// var person = {
//     name: "John",
//     surname: "Smith",
//     address: {
//         street: "123 Main Street",
//         city: "London"
//     }
// }


class Identity {
    constructor(name, surname) {
        this.empId = 1;
        this.name = name;
        this.surname = surname;
    }
}

class Employee {
    constructor() {
        this.identity = new Identity();
    }
}

class Project {
    constructor(employee) {
        this.manager = employee;
    }
}

let emp1 = new Employee();

let p1 = new Project(emp1);
p1 = null;
// console.log(p1.manager.identity.empId);

console.log(emp1.identity.empId);