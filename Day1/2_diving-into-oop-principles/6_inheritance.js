function Person(){
    this.name = "";
    this.surname = "";
}

function Developer() {
    this.knownLanguage = "";
}

Developer.prototype = new Person();

var johnSmith = new Developer();
johnSmith.name = "John";
johnSmith.surname = "Smith";
johnSmith.knownLanguage = "JavaScript";
console.log(johnSmith);