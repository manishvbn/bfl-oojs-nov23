// const company = {
//     name: "BFL",
//     employees: [],
//     sortEmployeesNyName: function () {
//         employees.sort(function (a, b) {
//             return a.name > b.name;
//         });
//     }
// };

// company.employees = "Hello";
// company.sortEmployeesNyName();

function Company(name) {
    var employees = [];
    this.name = name;

    this.getEmployees = function () {
        return employees;
    }

    this.addEmployee = function (employee) {
        employees.push(employee);
    }

    this.sortEmployeesNyName = function () {
        employees.sort(function (a, b) {
            return a.name > b.name;
        });
    }
}

let company = new Company();
