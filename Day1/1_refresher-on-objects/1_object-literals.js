// // var person1 = { name: "Manish", surname: "Sharma" };

// // var person1 = { "first-name": "Manish", "last-name": "Sharma" };

// // console.log(person1["first-name"]);
// // console.log(person1.first-name);

// var person1 = {
//     fname: "Manish",
//     lname: "Sharma",
//     address: {
//         street: "123 JS Street",
//         city: "JS City",
//     },
//     // showFullName: function() {
//     //     return (this.fname + " " + this.lname);
//     // }
//     // ES6 (ECMAScript 2015)
//     showFullName() {
//         return (this.fname + " " + this.lname);
//     }
// };

// console.log(person1.fname);
// person1.fname = "Abhijeet";
// console.log(person1.fname);

// person1.address.state = "MH";
// console.log(person1.address);

// delete person1.address;
// console.log(person1);

// let fullname = person1.showFullName();
// console.log(fullname);

var johnSmith = {
    name: "John",
    surname: "Smith",
    address: {
        street: "13 Duncannon Street",
        city: "London",
        country: "United Kingdom"
    },
    displayFullName: function () {
        return this.name + " " + this.surname;
    }
};

var marioRossi = {
    name: "Mario",
    surname: "Rossi",
    address: {
        street: "Piazza Colonna 370",
        city: "Roma",
        country: "Italy"
    },
    displayFullName: function () {
        return this.name + " " + this.surname;
    }
};