// var person = new Object();
// person.name = "John";
// person.surname = "Smith";

// var person1 = {};
// console.log(person1.constructor === Object);

// var n = 10;
// console.log(typeof n);

// var n1 = new Object(10);
// console.log(typeof n1);

// var n2 = Number(10);
// console.log(typeof n2);

// var n3 = new Number(10);
// console.log(typeof n3);

// var toy1 = new Object();
// var toy2 = new Object();

// console.log(Object.prototype);
// console.log(toy1.__proto__);        // Dunder Proto (Please donot use in the code)
// console.log(toy2.__proto__);

// console.log(toy1.__proto__ === toy2.__proto__);
// console.log(Object.prototype === toy1.__proto__);
// console.log(Object.prototype === toy2.__proto__);

var s = "Hello";
String.prototype.padLeft = function (width, char) {
    var result = this;
    char = char || " ";
    if (this.length < width) {
        result = new Array(width - this.length + 1).join(char) + this;
    }
    return result;
}

console.log(s.padLeft(10, "x"));
console.log("abc".padLeft(10, "x"));