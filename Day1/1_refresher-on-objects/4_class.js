class Person {
    constructor(name, surname) {
        this.name = name;
        this.surname = surname;
    }
}

// Old Style Code
// function Person(name, surname) {
//     "use strict";
//     this.name = name;
//     this.surname = surname;
// }

// console.log(typeof Person);
var p = new Person();
console.log(p);