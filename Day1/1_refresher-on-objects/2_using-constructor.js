// // Constructor Function
// function Person() {
//     "use strict";
//     // Data Property
//     this.name = "";
//     this.surname = "";
//     this.address = {
//         street: "",
//         city: "",
//         country: ""
//     };
//     this.email = "";
//     this.displayFullName = function () {
//         return this.name + " " + this.surname;
//     };
// }

// var johnSmith = new Person();
// console.log(johnSmith);

// var marioRossi = new Person();
// console.log(marioRossi);

// var johnSmith = Person();       // Runtime Error
// console.log(johnSmith);

var ns = {
    Person: function () {
        "use strict";
        console.log(this);
        // Data Property
        this.name = "";
        this.surname = "";
        this.address = {
            street: "",
            city: "",
            country: ""
        };
        this.email = "";
        this.displayFullName = function () {
            return this.name + " " + this.surname;
        };
    }
}

var johnSmith = ns.Person();
console.log(johnSmith);